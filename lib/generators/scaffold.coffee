#
# Created by Mike Linde <mlinde@lintechsol.com> on 25/05/15.
#

Promise = require 'bluebird'
ModelGenerator = require './model'
ControllerGenerator = require './controller'
ViewsGenerator = require './views'

class ScaffoldGenerator
  constructor: (@newItem, @projectDir) ->

  createModel = (newItem, projectDir) ->
    generator = new ModelGenerator newItem, projectDir
    generator.run()

  createController = (newItem, projectDir) ->
    generator = new ControllerGenerator newItem, projectDir
    generator.run()

  createViews = (newItem, projectDir) ->
    generator = new ViewsGenerator newItem, projectDir
    generator.run()

  run: ->
    _self = @

    projectDir = _self.projectDir
    newItem = _self.newItem

    return new Promise (resolve, reject) ->
      createModel newItem, projectDir
      .then -> createController newItem, projectDir
      .then -> createViews newItem, projectDir
      .then ->
        resolve()
      .catch (err) ->
        reject err

module.exports = ScaffoldGenerator