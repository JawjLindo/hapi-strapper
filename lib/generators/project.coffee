#
# Created by Mike Linde <mlinde@lintechsol.com> on 24/05/15.
#
Promise = require 'bluebird'
mkdirp = Promise.promisify require 'mkdirp'
fs = Promise.promisifyAll require 'fs-extra'
_ = require 'underscore'
path = require 'path'
Handlebars = require 'handlebars'
moment = require 'moment'

class ProjectGenerator
  templateDir = path.normalize "#{__dirname}/../../templates"

  constructor: (@projectName, @projectDirParent) ->
    @projectDir = path.join @projectDirParent, @projectName

  createDir = (projectName, projectDirParent, projectDir) ->
    return new Promise (resolve, reject) ->
      fs.readdirAsync projectDirParent
      .then (files) ->
        if _.contains files, projectName
          return reject "The directory #{projectName} already exists. Cannot create the project with this name."

        mkdirp projectDir
        .then ->
          console.log "Directory created: #{projectDir}"
          resolve()
        .catch (err) ->
          reject err
      .catch (err) ->
        reject err

  createLicense = (projectDir) ->
    return new Promise (resolve, reject) ->
      templateFile = path.normalize "#{templateDir}/LICENSE.handlebars"
      fs.readFileAsync templateFile, {encoding: 'utf8'}
      .then (content) ->
        template = Handlebars.compile content
        data =
          year: moment().year()
        licenseFile = path.normalize "#{projectDir}/LICENSE"
        fileContent = template data
        fs.writeFileAsync licenseFile, fileContent, encoding: 'utf8'
        .then ->
          console.log "File created: #{licenseFile}"
          resolve()
        .catch (err) ->
          reject err
      .catch (err) ->
        reject err

  createPackage = (projectName, projectDir) ->
    return new Promise (resolve, reject) ->
      templateFile = path.normalize "#{templateDir}/package.json"
      packageFile = path.normalize "#{projectDir}/package.json"

      packageContent = require templateFile
      packageContent.name = projectName
      packageContent.version = '0.0.1'
      packageContent.description = "#{projectName} created by hapi-strapper"
      packageJson = JSON.stringify packageContent, null, '\t'
      fs.writeFileAsync packageFile, packageJson, encoding: 'utf8'
      .then ->
        console.log "File created: #{packageFile}"
        resolve()
      .catch (err) ->
        reject err

  createReadme = (projectName, projectDir) ->
    return new Promise (resolve, reject) ->
      templateFile = path.normalize "#{templateDir}/README.md.handlebars"
      fs.readFileAsync templateFile, {encoding: 'utf8'}
      .then (content) ->
        template = Handlebars.compile content
        data =
          projectName: projectName
        readmeFile = path.normalize "#{projectDir}/README.md"
        fileContent = template data
        fs.writeFileAsync readmeFile, fileContent, encoding: 'utf8'
        .then ->
          console.log "File created: #{readmeFile}"
          resolve()
        .catch (err) ->
          reject err
      .catch (err) ->
        reject err

  createGitIgnore = (projectDir) ->
    return new Promise (resolve, reject) ->
      templateFile = path.normalize "#{templateDir}/.gitignore.template"
      gitIgnoreFile = path.normalize "#{projectDir}/.gitignore"
      fs.copyAsync templateFile, gitIgnoreFile
      .then ->
        console.log "File created: #{gitIgnoreFile}"
        resolve()
      .catch (err) ->
        reject err

  createBowerFiles = (projectName, projectDir) ->
    return new Promise (resolve, reject) ->
      templateFile = path.normalize "#{templateDir}/bower.json"
      bowerFile = path.normalize "#{projectDir}/bower.json"

      bowerContent = require templateFile
      bowerContent.name = projectName
      bowerContent.version = '0.0.1'
      bowerContent.description = "#{projectName} created by hapi-strapper"
      bowerJson = JSON.stringify bowerContent, null, '\t'
      fs.writeFileAsync bowerFile, bowerJson, encoding: 'utf8'
      .then ->
        console.log "File created: #{bowerFile}"
      .then ->
        templateFile = path.normalize "#{templateDir}/.bowerrc.template"
        bowerrcFile = path.normalize "#{projectDir}/.bowerrc"
        fs.copyAsync templateFile, bowerrcFile
        .then ->
          console.log "File created: #{bowerrcFile}"
          resolve()
        .catch (err) ->
          return reject err
      .catch (err) ->
        reject err

  createGulpFiles = (projectDir) ->
    return new Promise (resolve, reject) ->
      files = [
        'gulpfile.js'
        'the-gulpfile.coffee'
      ]
      for file in files
        do (file) ->
          template = path.normalize "#{templateDir}/#{file}"
          target = path.normalize "#{projectDir}/#{file}"
          fs.copyAsync template, target
      Promise.all files
      .then ->
        console.log 'Files created: Gulp files'
        resolve()
      .catch (err) ->
        reject err

  createConfigurationFiles = (projectDir) ->
    return new Promise (resolve, reject) ->
      templatesToCopy = path.normalize "#{templateDir}/config"
      targetDir = path.normalize "#{projectDir}/config"
      fs.copyAsync templatesToCopy, targetDir
      .then ->
        console.log 'Files created: Configuration files'
        resolve()
      .catch (err) ->
        reject err

  createApplicationFiles = (projectDir) ->
    return new Promise (resolve, reject) ->
      templatesToCopy = path.normalize "#{templateDir}/app"
      targetDir = path.normalize "#{projectDir}/app"
      fs.copyAsync templatesToCopy, targetDir
      .then ->
        console.log 'Files created: Application files'
        resolve()
      .catch (err) ->
        reject err

  createTestFiles = (projectDir) ->
    return new Promise (resolve, reject) ->
      templatesToCopy = path.normalize "#{templateDir}/test"
      targetDir = path.normalize "#{projectDir}/test"
      fs.copyAsync templatesToCopy, targetDir
      .then ->
        console.log 'Files created: Test files'
        resolve()
      .catch (err) ->
        reject err

  runNpm = (projectDir) ->
    return new Promise (resolve, reject) ->
      console.log 'Running \'npm install\'. Please wait until it is complete...\n'
      spawn = require('child_process').spawn
      proc = spawn 'npm', ['install'], {
        stdio: 'inherit'
        cwd: projectDir
      }
      proc.on 'error', (error) ->
        reject error
      proc.on 'close', ->
        resolve()

  run: ->
    _self = @
    return new Promise (resolve, reject) ->
      createDir _self.projectName, _self.projectDirParent, _self.projectDir
      .then -> createLicense _self.projectDir
      .then -> createPackage _self.projectName, _self.projectDir
      .then -> createReadme _self.projectName, _self.projectDir
      .then -> createGitIgnore _self.projectDir
      .then -> createBowerFiles _self.projectName, _self.projectDir
      .then -> createGulpFiles _self.projectDir
      .then -> createConfigurationFiles _self.projectDir
      .then -> createApplicationFiles _self.projectDir
      .then -> createTestFiles _self.projectDir
      .then -> runNpm _self.projectDir
      .then -> resolve()
      .catch (err) -> reject err

module.exports = ProjectGenerator