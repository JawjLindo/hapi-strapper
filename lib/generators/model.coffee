#
# Created by Mike Linde <mlinde@lintechsol.com> on 24/05/15.
#
require('../../templates/helpers')()
Promise = require 'bluebird'
mkdirp = Promise.promisify require 'mkdirp'
path = require 'path'
fs = Promise.promisifyAll require 'fs'
pluralize = require 'pluralize'
Handlebars = require 'handlebars'

class ModelGenerator
  templateDir = path.normalize "#{__dirname}/../../templates"

  constructor: (@newItem, @projectDir) ->

  createModel = (newItem, projectDir) ->
    return new Promise (resolve, reject) ->
      modelsDir = path.normalize "#{projectDir}/app/models"
      modelPath = path.normalize "#{modelsDir}/#{newItem}.coffee"
      mkdirp modelsDir
      .then -> fs.readFileAsync path.normalize("#{templateDir}/model.handlebars"), encoding: 'utf8'
      .then (data) ->
        template = Handlebars.compile data
        result = template
          item: newItem
          itemCapitalized: newItem.capitalize()
        fs.writeFileAsync modelPath, result
        .then ->
          resolve()
      .catch (err) ->
        reject err

  run: ->
    _self = @
    return new Promise (resolve, reject) ->
      createModel _self.newItem, _self.projectDir
      .then ->
        console.log "Created: app/models/#{_self.newItem}.coffee"
        resolve()
      .catch (err) -> reject err



module.exports = ModelGenerator