#
# Created by Mike Linde <mlinde@lintechsol.com> on 25/05/15.
#
require('../../templates/helpers')()
Promise = require 'bluebird'
mkdirp = Promise.promisify require 'mkdirp'
path = require 'path'
fs = Promise.promisifyAll require 'fs'
pluralize = require 'pluralize'
Handlebars = require 'handlebars'
ncp = Promise.promisify require('ncp').ncp

class ViewsGenerator
  templateDir = path.normalize "#{__dirname}/../../templates"

  constructor: (@newItem, @projectDir) ->

  createViews = (newItem, newItemPlural, projectDir) ->
    return new Promise (resolve, reject) ->
      viewsDir = path.normalize "#{projectDir}/app/views/#{newItemPlural}"
      mkdirp viewsDir
      .then -> ncp path.normalize("#{templateDir}/views"), path.normalize("#{viewsDir}")
      .then ->
        console.log "Created: app/views/#{newItemPlural}/index.handlebars"
        console.log "Created: app/views/#{newItemPlural}/show.handlebars"
        console.log "Created: app/views/#{newItemPlural}/new.handlebars"
        console.log "Created: app/views/#{newItemPlural}/edit.handlebars"
        resolve()
      .catch (err) ->
        reject err

  run: ->
    _self = @

    projectDir = _self.projectDir
    newItem = _self.newItem
    newItemPlural = pluralize.plural newItem

    return new Promise (resolve, reject) ->
      createViews newItem, newItemPlural, projectDir
      .then -> resolve()
      .catch (err) -> reject err

module.exports = ViewsGenerator
